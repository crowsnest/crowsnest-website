---
# Feel free to add content and custom Front Matter to this file.

layout: default
---

<div class="bg-white shadow sm:rounded-lg">
  <div class="px-4 py-5 sm:p-6">
    <div class="sm:flex sm:items-start sm:justify-between">
      <div>
        <h3 class="text-xl leading-6 font-bold text-gray-900">
          Thank you for subscribing!
        </h3>
        <div class="mt-2 max-w-xl text-base leading-5 text-gray-500">
          <p>
            We'll let you know once a private beta is ready for trial.
          </p>
        </div>
      </div>
      <div class="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
        <span class="inline-flex rounded-md shadow-sm">
          <a href="/" class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-primary-500 hover:bg-primary-500 focus:outline-none focus:border-primary-700 focus:shadow-outline-primary active:bg-primary-700 transition ease-in-out duration-150">
            Go Back
          </a>
        </span>
      </div>
    </div>
  </div>
</div>
