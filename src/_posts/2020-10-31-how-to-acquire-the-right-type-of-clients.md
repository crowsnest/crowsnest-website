---
layout: post
title: How To Acquire (the Right Type of) Clients
description: >-
  I’m getting asked this question over and over again: How did you manage to get
  so many clients you felt the need to build a smart scheduling software in the
  first place? To that question, there’s only one answer, as simple as it
  sounds: Word of mouth.


  Produce valuable outcomes for your clients and develop a broad skillset. Try to find (tech) agencies that will “manage” the actual customers for you and provide you with actionable workloads.
image: /images/uploads/556fef1f-f837-43b3-975d-dd12708d79b1.jpeg
categories:
  - Product
author: julian@julianrubisch.at
date: 2020-11-01T07:07:00.000Z
---
I’m getting asked this question over and over again: How did you manage to get so many clients you felt the need to build a smart scheduling software in the first place?\
\
Well, I will try to sum up my experiences to maybe give some guidance as well as shed some light on my motivation.\
\
First, I don’t have that many clients. I actually can count them on both of my hands, and that is no accident. I like to keep that number small and manageable, but I’ll get to that later.

![Co-Working Space]({{ site.url }}/images/uploads/556fef1f-f837-43b3-975d-dd12708d79b1.jpeg "Foto Credit: Unsplash")

### Acquisition

To answer the question *“how did you acquire them?”* there’s only one answer, as simple as it sounds: **Word of mouth.**

Certainly, there’s still a lot of variety in that answer, so let’s break it apart. “Word of mouth” is actually a funnel for different sources, still:

* Clients that I was recommended to by other clients
* Clients that I was recommended to by fellow developers
* Clients that approach me through my open source contributions

I cannot provide any advice on how to be recommended other than: Produce valuable outcomes for your clients. What certainly helps is to develop a broad skillset with distinct “singularities” of specialized knowledge - the proverbial **[T shaped personality](https://en.m.wikipedia.org/wiki/T-shaped_skills)**. In my experience - I can only account for software engineering - I usually encounter either generalists without specialization or specialists without much general knowledge. If you’re a Ruby developer, learn JavaScript. If you’re a JavaScript developer, learn a ”backend” language. If you’re a “full-stack developer”, look at data science, etc. Conversely, if you just graduated and don’t know where to specialize, try to find a community that is both welcoming and provides ample learning resources. Both approaches will pay off in the end!

![Connected lightbulbs]({{ site.url }}/images/uploads/229a25f5-1f7c-49c9-8c07-d12be24ac1c2.jpeg "Foto Credit: Unsplash")

### Proxy Clients

There’s something that’s probably typical of my industry (software engineering) and not so much other sectors: The fact that much work is getting relayed to me via agencies or software shops where I act as a subcontractor. These **proxy clients** as I will call them in this context take the place of Fiverr, Upwork, or similar platforms and help me focus on actual work being done instead of always chasing the next gig.\
\
These clients who represent other customers are also the reason why I must be able to **communicate my availability clearly and reliably** - my principal motivation to create this product.

When analyzing my past workloads I [found patterns](https://www.crowsnestapp.com/data-science/2020/09/24/the-science-behind-crows-nests-burn-rate-types/) emerging especially discernible [between types of projects](https://www.crowsnestapp.com/product/2020/08/30/hidden-overbookings-freelancers-fast-lane-to-burn-out/) (one-offs vs. retainers, development vs consulting, etc.). So I figured, to convey my availability more accurately, I want a tool that estimates my daily workloads based on those patterns. A bonus would be if the system would be smart enough to recognize patterns once a workload is started and predict how it will proceed.

Conventional calendars don’t serve that purpose well, and homegrown spreadsheets lack the ability to conveniently link them to time tracking software. Research of comparable freelancer SaaSes such as [Cushion](http://cushionapp.com/) revealed that they’re often feature-over-complete but don’t allow for intelligent scheduling. For example, personally I don’t need a finance planning software, I use a combination of [Harvest forecasts](https://www.getharvest.com/forecast) and exports from my bookkeeping software for that.

The **job to be done** is: ”help me find availability spots in my calendar and avoid burnout”, not “show me how much I will earn”. In my case, the latter is covered by other tooling, but your mileage may vary, of course.

To get in that comfortable position of a) having enough work and b) being able to actually get it done, try to find (tech) agencies that will “manage” the actual customers for you (negotiating requirements, hourly rates, deadlines etc.) and provide you with **actionable workloads**. This is certainly a question of culture and has to be actively developed - maybe a topic for a future explorative blog post.