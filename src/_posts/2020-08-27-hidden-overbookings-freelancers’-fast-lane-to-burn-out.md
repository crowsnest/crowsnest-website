---
layout: post
title: Hidden Overbookings - Freelancers’ Fast Lane to Burn Out
description: Traditional planning software lets you allocate time over a certain
  timespan, but that's not how a typical project passes off. Crow’s Nest sets
  out to remedy this shortcoming by allowing different types of burn rates into
  the equation, and taking advantage of your time tracking data.
image: /images/uploads/5a3f6d4c-9623-4044-9e70-a7b354f825e3.jpeg
categories:
  - Product
author: julian@julianrubisch.at
date: 2020-08-30T08:08:46.681Z
---
I’ve been a freelance software engineer for 7 years now, and had the immense luck of being fully booked most of the time. My primary source of stress was too much work, not the lack thereof.

During the COVID lockdown, I couldn’t take on as much work, so I was left with some “spare” time to think about my work practice and started to see patterns. One of my epiphanies was related to workload distribution.

### Burn Rates Aren't Linear

Traditional planning software (or your homegrown Excel sheet, for that matter) lets you allocate time over a certain timespan, but that's not how a typical project passes off.

There are several reasons why work doesn't happen linearly, here are some usual suspects:

#### Sprint-, or Cycle-Type Work

This - at least for me - typically happens with retainers. Once a month I meet the client to demo what has been done and prioritize new work packages. While all is fresh in memory, I start executing tasks and implementing features. In the middle of the month, some other project needs my attention, and work grinds to a halt. Then, right before the next "sprint review" meeting, I work off all remaining tasks.

Here's how that looks like in an hour-per-week bar chart. The pink arrows denote weeks with meetings.

![Bar chart showing work hours per week]({{ site.url }}/images/uploads/ipnext_–_minthesize_e_u__–_harvest.png "Cyclic Project Burn Rate")

#### Waiting for the Client to Hand Over Assets

We've all been there. Prerequisites and requirements for a project are discussed at great length, you start to build wireframes, craft a first prototype. Because there are no production assets yet, you fill in the gaps with all sorts of fake data: Lorem text, unsplash images, dummy CSV data etc.

Weeks pass, and despite urging the client to send you some data, nothing happens. Further development is stalled. When the finalized and signed off assets finally arrive, they hold all sorts of surprises:

* CSVs don’t match agreed upon specifications
* Images come in the wrong formats (a classic: JPGs without alpha instead of transparent PNGs)
* Text arrives unformatted, etc.

So, to deliver on time, you start hustling and adapting everything until it fits the your project’s criteria. Below are two typical examples of how workloads distribute over time when your bottleneck is waiting for client data to arrive:

![Bar chart showing work hours per week]({{ site.url }}/images/uploads/henkel_minigames_–_minthesize_e_u__–_harvest.png "Increasing Burn Rate 1")

![Bar chart showing work hours per week]({{ site.url }}/images/uploads/vaude-touch_–_minthesize_e_u__–_harvest.png "Increasing Burn Rate 2")

### Hidden Overbookings

Hidden overbookings happen when you unintentionally pile up work with incompatible burn rates.

So while your scheduling app or calendar is showing you this:

![Calendar Entries for Two Projects]({{ site.url }}/images/uploads/b90811eb-a615-4ff4-ba88-2fb25e920a69.jpeg "Calendar Entries for Two Projects")

What’s actually happening is only revealed if you take a closer look at the accumulated burn rates:

![Two Projects’ Colliding Burn Rates]({{ site.url }}/images/uploads/5a3f6d4c-9623-4044-9e70-a7b354f825e3.jpeg "Two Projects’ Colliding Burn Rates")

Without noticing, you overbooked your schedule and unwittingly agreed to a massive, predictable amount of stress that could have easily been avoided.

Now you could argue that work can always be shifted back and forth in time, filling in the gaps in your calendar. Practice shows, that that’s not always possible. Some dates may be fixed (take trade fairs as an example), other times you simply depend on external processes. The heteronomous powers affecting your schedule far outnumber your personal preferences.

### Take Control Over Your Schedule

[Crow’s Nest](https://www.crowsnestapp.com) sets out to remedy this shortcoming by

1. allowing different types of burn rates into the equation, and
2. taking advantage of your time tracking data.

Let me explain how this works:

#### Leverage Burn Rate Types

In the simplest case, you know beforehand what kind of project, and thus workload distribution you’re dealing with. Choose between

* Constant
* Valley
* Hill
* Decreasing
* Increasing

to model your bookings.

#### Integrate Your Time Tracking Data

There are two ways in which including your time tracking data can help you avoid overbookings:

1. **Analyze past projects and use average workload distribution over time** to predict new projects. Since patterns are often client-specific, this is very helpful for clients that book you **repeatedly**.

2. **Analyze the characteristic** of running projects and **predict their further development**. Thus you can spot bottlenecks faster and make reliable assertions about your availability.
