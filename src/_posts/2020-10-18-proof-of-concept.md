---
layout: post
title: Proof of Concept, Part One
description: I've spent the last four weeks crafting a proof of concept to show
  that what I plan is actually doable, and more importantly, that it can be done
  by me. The backbone of the app, of course, is the ability to add a workload to
  your schedule. Another must-have is that scheduling should remain flexible at
  all times. For example, you might want to change your time precision from
  quarter hours to hours, and the workload should self-adapt.
image: /images/uploads/crowsnest-add-workload.gif
categories:
  - Product
author: julian@julianrubisch.at
date: 2020-10-18T17:44:31.638Z
---
I've spent the last four weeks crafting a proof of concept to show that what I plan is actually doable, and more importantly, that it can be done by *me*. 

Technically, undoubtedly the hardest part of it all was to get the visualization right, but I'll leave this topic for another technical blog post later. Instead I'm going to showcase what features of the product I managed to build so far.

### Adding a Workload

The backbone of the app, of course, is the ability to add a workload to your schedule. The process is as follows: You choose a project (which will later be filterable by client), enter an amount of hours, a start end an end date. Then, you select a workload type. I've implemented *constant, increasing* and *decreasing* in this first batch.

The algorithm then allocates a discrete amount of hours per day, respecting weekends. Allocated workloads are displayed as a variation of an *area chart*:

![Animation of adding a workload to your schedule](/images/uploads/crowsnest-add-workload.gif "Adding a workload to your schedule")

### Adapt Your Scheduling Precision

Another must-have is that scheduling should remain flexible at all times. For example, you might want to change your time precision from quarter hours to hours, and the workload should self-adapt. Given the fact that rounding errors will occur and have to be corrected, this was a non-trivial undertaking.

The animation below shows that this goal can be met: The allocated time boxes shift according to the specified precision, keeping the characteristic (increasing/decreasing) and the total hours per workload intact.

![Animation of changing the scheduling precision](/images/uploads/crowsnest-settings-precision.gif "Changing the scheduling precision")

### Respect Regional Holidays

What's a good schedule without sufficient days off? Of course, the allocation algorithm should remain weekends and holidays work-free. You as a user should be able to set the country of your residence and the schedule should adapt.

In the future you will of course be able to plan individual days off, too.

![Animation of changing the holiday region](/images/uploads/crowsnest-holiday-region.gif "Changing the holiday region")

### Further Progress

Next on my *schedule* 😉😉😉 are

* Valley and Hill Types
* Editing and Deleting Workloads
* Improvements to the Visualization
