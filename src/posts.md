---
layout: page
title: Blog Posts
permalink: /posts/
---

<div class="grid gap-5 md:grid-cols-2">
  {% for post in site.posts %}
    {% render "post_card", post: post %}
  {% endfor %}
</div>

<!-- If you have a lot of posts, you may want to consider adding [pagination](https://www.bridgetownrb.com/docs/content/pagination)! -->
