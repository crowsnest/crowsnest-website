class Filters < SiteBuilder
  def build
    liquid_filter "md5" do |input|
      Digest::MD5.hexdigest input.strip
    end

    liquid_filter "find_author_by_email", :find_author_by_email
  end

  def find_author_by_email(email)
    site.collections["authors"]
      .select { |author| author.email == email }
      .first
  end
end
