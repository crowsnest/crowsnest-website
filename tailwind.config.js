module.exports = {
  purge: {
    mode: "production",
    content: ["./**/*.html", "./**/*.liquid", "./**/*.md", "./**/*.erb"],
    options: {
      whitelist: [".highlight"],
      whitelistPatterns: [/highlight/],
      whitelistPatternsChildren: [/highlight/],
    }
  },
  theme: {
    fontFamily: {
      display: ["Barlow Semi Condensed", "Oswald"],
      sans: [
        "Barlow",
        "system-ui",
        "-apple-system",
        "BlinkMacSystemFont",
        "Segoe UI",
        "Roboto",
        "Helvetica Neue",
        "Arial",
        "Noto Sans",
        "sans-serif",
        "Apple Color Emoji",
        "Segoe UI Emoji",
        "Segoe UI Symbol",
        "Noto Color Emoji"
      ]
    },
    typography: theme => ({
      default: {
        css: {
          h3: {
            color: theme("colors.primary-900")
          },
          h4: {
            color: theme("colors.primary-900")
          }
        }
      }
    }),
    extend: {
      colors: {
        primary: {
          50: '#F9F6FF',
          100: '#F4EEFF',
          200: '#E2D4FF',
          300: '#D1BAFF',
          400: '#AF86FF',
          500: '#8C52FF',
          600: '#7E4AE6',
          700: '#543199',
          800: '#3F2573',
          900: '#2A194D'
        },
      },
      maxWidth: {
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%"
      }
    }
  },
  variants: {},
  plugins: [require("@tailwindcss/ui")]
};
